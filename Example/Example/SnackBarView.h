//
//  RSBSnackBarView.h
//  BannersSample
//
//  Created by Evgeny Mikhaylov on 12/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SnackBarView : UIView

@property (nonatomic, readonly) UILabel *messageLabel;
@property (nonatomic, readonly) UIButton *button;

@end
