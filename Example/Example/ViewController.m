//
//  ViewController.m
//  BannersSample
//
//  Created by Evgeny Mikhaylov on 12/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ViewController.h"
#import "SnackBarView.h"
#import "RSBSnacks.h"

@interface ViewController ()

@property (nonatomic) UIButton *showButton;
@property (nonatomic) UIButton *hideButton;
@property (nonatomic) UIButton *nextButton;
@property (nonatomic) UILabel *queueLabel;
@property (nonatomic) UISwitch *queueSwitch;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.showButton = [[UIButton alloc] init];
    [self.showButton setTitle:@"Show Snack" forState:UIControlStateNormal];
    [self.showButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.showButton setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    [self.showButton addTarget:self action:@selector(showButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.showButton sizeToFit];
    [self.view addSubview:self.showButton];
    
    self.hideButton = [[UIButton alloc] init];
    [self.hideButton setTitle:@"Hide Snack" forState:UIControlStateNormal];
    [self.hideButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.hideButton setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    [self.hideButton addTarget:self action:@selector(hideButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.hideButton sizeToFit];
    [self.view addSubview:self.hideButton];

    self.nextButton = [[UIButton alloc] init];
    [self.nextButton setTitle:@"Next View Controller" forState:UIControlStateNormal];
    [self.nextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.nextButton setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    [self.nextButton addTarget:self action:@selector(nextButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton sizeToFit];
    [self.view addSubview:self.nextButton];

    self.queueLabel = [[UILabel alloc] init];
    self.queueLabel.text = @"Snack queue enabled";
    [self.queueLabel sizeToFit];
    [self.view addSubview:self.queueLabel];
    
    self.queueSwitch = [[UISwitch alloc] init];
    self.queueSwitch.on = NO;
    [self.queueSwitch addTarget:self action:@selector(queueSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.queueSwitch];

    RSBSnacks.maxSupportedWindowLevel = UIWindowLevelStatusBar + 1;
    RSBSnacks.snackBarsQueueEnabled = self.queueSwitch.isOn;
    RSBSnacks.snackBarVisibilityDuration = 2.0;
}

#pragma mark - Layout

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.showButton.frame = (CGRect){
        .origin.x = 0.0,
        .origin.y = 0.0,
        .size.width = CGRectGetWidth(self.showButton.frame),
        .size.height = 50.0,
    };
    self.showButton.center = (CGPoint){
        .x = 0.2 * CGRectGetWidth(self.view.frame),
        .y = 0.4 * CGRectGetHeight(self.view.frame),
    };

    self.hideButton.frame = (CGRect){
        .origin.x = 0.0,
        .origin.y = 0.0,
        .size.width = CGRectGetWidth(self.hideButton.frame),
        .size.height = 50,
    };
    self.hideButton.center = (CGPoint){
        .x = 0.8 * CGRectGetWidth(self.view.frame),
        .y = 0.4 * CGRectGetHeight(self.view.frame),
    };
    
    self.nextButton.frame = (CGRect){
        .origin.x = 0.0,
        .origin.y = 0.0,
        .size.width = CGRectGetWidth(self.nextButton.frame),
        .size.height = 50,
    };
    self.nextButton.center = (CGPoint){
        .x = 0.5 * CGRectGetWidth(self.view.frame),
        .y = 0.6 * CGRectGetHeight(self.view.frame),
    };
    
    self.queueSwitch.center = (CGPoint){
        .x = 0.5 * CGRectGetWidth(self.view.frame),
        .y = 0.3 * CGRectGetHeight(self.view.frame),
    };
    
    self.queueLabel.center = (CGPoint){
        .x = self.queueSwitch.center.x,
        .y = CGRectGetMinY(self.queueSwitch.frame) - CGRectGetHeight(self.queueLabel.frame) - 5.0,
    };

}

#pragma mark - Actions

- (void)snackButtonPressed:(id)sender {
    NSLog(@"SNACK BUTTON PRESSED!");
    [RSBSnacks hideActiveSnackBar];
}

- (void)showButtonPressed:(id)sender {
    NSLog(@"SHOW SNACK!");
    SnackBarView *snackBarView = [[SnackBarView alloc] init];
    snackBarView.backgroundColor = [UIColor colorWithRed:(arc4random()%255)/255.0f green:(arc4random()%255)/255.0f blue:(arc4random()%255)/255.0f alpha:1.0f];
    snackBarView.messageLabel.text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
    snackBarView.messageLabel.numberOfLines = 0;
    [snackBarView.button addTarget:self action:@selector(snackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [RSBSnacks showSnackBarWithContentView:snackBarView hideBlock:^{
        NSLog(@"SNACK HIDDEN!");
    }];
}

- (void)hideButtonPressed:(id)sender {    
    NSLog(@"HIDE SNACK!");
    [RSBSnacks hideActiveSnackBar];
}

- (void)nextButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"second" sender:nil];
}

- (void)queueSwitchValueChanged:(id)sender {
    RSBSnacks.snackBarsQueueEnabled = self.queueSwitch.isOn;
}

@end
