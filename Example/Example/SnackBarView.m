//
//  RSBSnackBarView.m
//  BannersSample
//
//  Created by Evgeny Mikhaylov on 12/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "SnackBarView.h"

@interface SnackBarView ()

@property (nonatomic) UILabel *messageLabel;
@property (nonatomic) UIButton *button;

@end

@implementation SnackBarView

- (void)dealloc {
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {        
        self.messageLabel = [[UILabel alloc] init];
        self.messageLabel.numberOfLines = 1;
        self.messageLabel.textColor = [UIColor whiteColor];
        self.messageLabel.layer.shadowColor = [UIColor blackColor].CGColor;
        self.messageLabel.layer.shadowOpacity = 1.0;
        self.messageLabel.layer.shadowRadius = 1.0;
        self.messageLabel.layer.shadowOffset = CGSizeZero;
        [self addSubview:self.messageLabel];
        
        self.button = [[UIButton alloc] init];
        self.button.frame = (CGRect){
            .origin.x = 0.0,
            .origin.y = 0.0,
            .size.width = 70,
            .size.height = 50,
        };
        self.button.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
        self.button.titleLabel.layer.shadowOpacity = 1.0;
        self.button.titleLabel.layer.shadowRadius = 1.0;
        self.button.titleLabel.layer.shadowOffset = CGSizeZero;
        [self.button setTitle:@"Button" forState:UIControlStateNormal];
        [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.button setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
        [self addSubview:self.button];
    }
    return self;
}

#pragma mark - Layout

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize labelSize = [self.messageLabel sizeThatFits:(CGSize) {
        .width = size.width - CGRectGetWidth(self.button.frame),
        .height = size.height,
    }];
    CGSize resultSize = (CGSize) {
        .width = size.width,
        .height = labelSize.height,
    };
    return resultSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.button.center = (CGPoint){
        .x = CGRectGetWidth(self.frame) - CGRectGetWidth(self.button.frame) / 2.0,
        .y = CGRectGetHeight(self.frame) / 2.0,
    };
    self.messageLabel.frame = (CGRect){
        .origin.x = 0.0,
        .origin.y = 0.0,
        .size.width = CGRectGetMinX(self.button.frame),
        .size.height = CGRectGetHeight(self.frame),
    };
}

@end
