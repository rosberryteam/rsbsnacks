//
//  AppDelegate.h
//  BannersSample
//
//  Created by Evgeny Mikhaylov on 12/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

