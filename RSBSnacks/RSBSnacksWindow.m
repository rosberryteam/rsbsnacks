//
//  RSBSnacksWindow.m
//  Pods
//
//  Created by Evgeny Mikhaylov on 10/11/2016.
//
//

#import "RSBSnacksWindow.h"
#import "RSBSnacksViewController.h"
#import <objc/runtime.h>

@interface UIView (RSBSnackBarsContainerView)

@property (nonatomic) BOOL snackBarContentViewVisible;
@property (nonatomic, weak) UIView *snackBarBackgroundView;
@property (nonatomic, copy) void(^snackBarContentViewDidHideBlock)();

@end

@implementation UIView (RSBSnackBarsContainerView)

- (void)setSnackBarContentViewVisible:(BOOL)snackBarContentViewVisible {
    objc_setAssociatedObject(self, @selector(snackBarContentViewVisible), @(snackBarContentViewVisible), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)snackBarContentViewVisible {
    return [objc_getAssociatedObject(self, @selector(snackBarContentViewVisible)) boolValue];
}

- (void)setSnackBarBackgroundView:(UIView *)snackBarBackgroundView {
    objc_setAssociatedObject(self, @selector(snackBarBackgroundView), snackBarBackgroundView, OBJC_ASSOCIATION_ASSIGN);
}

- (UIView *)snackBarBackgroundView {
    return objc_getAssociatedObject(self, @selector(snackBarBackgroundView));
}

- (void)setSnackBarContentViewDidHideBlock:(void (^)())snackBarContentViewDidHideBlock {
    objc_setAssociatedObject(self, @selector(snackBarContentViewDidHideBlock), snackBarContentViewDidHideBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)())snackBarContentViewDidHideBlock {
    return objc_getAssociatedObject(self, @selector(snackBarContentViewDidHideBlock));
}

@end

@interface RSBSnacksWindow ()

@property (nonatomic) NSMutableArray<UIView *> *snackBarContentViews;
@property (nonatomic, weak) UIView *activeSnackBarContentView;

@property (nonatomic) NSTimer *timer;

@end

@implementation RSBSnacksWindow

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.windowLevel = UIWindowLevelStatusBar + 1;
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
        [self addGestureRecognizer:tapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
        [self addGestureRecognizer:swipeGestureRecognizer];
    }
    return self;
}

#pragma mark - Setters/Getters

- (NSMutableArray<UIView *> *)snackBarContentViews {
    if (!_snackBarContentViews) {
        _snackBarContentViews = [[NSMutableArray alloc] init];
    }
    return _snackBarContentViews;
}

- (BOOL)snackBarIsVisible {
    return (self.activeSnackBarContentView != nil);
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    [self layoutSnackBarContentView:self.activeSnackBarContentView];
}

- (void)layoutSnackBarContentView:(UIView *)contentView {
    CGFloat animationOffset = 10.0;
    CGSize snackBarViewSize = [contentView sizeThatFits:self.frame.size];
    contentView.snackBarBackgroundView.frame = (CGRect){
        .origin.x = 0,
        .origin.y = 0,
        .size.width = CGRectGetWidth(self.frame),
        .size.height = animationOffset + snackBarViewSize.height,
    };
    contentView.frame = (CGRect){
        .origin.x = 0,
        .origin.y = animationOffset,
        .size.width = CGRectGetWidth(contentView.snackBarBackgroundView.frame),
        .size.height = snackBarViewSize.height,
    };
    CGFloat backgroundViewHeight = CGRectGetHeight(contentView.snackBarBackgroundView.frame);
    contentView.snackBarBackgroundView.center = (CGPoint){
        .x = contentView.snackBarBackgroundView.center.x,
        .y = contentView.snackBarContentViewVisible ? (backgroundViewHeight / 2.0 - animationOffset) : (- backgroundViewHeight / 2.0),
    };
}

#pragma mark - Actions

- (void)timerAction:(id)sender {
    [self hideActiveSnackBarView];
    [self.timer invalidate];
    self.timer = nil;
}

- (void)tapAction:(id)sender {
    [self hideActiveSnackBarView];
}

- (void)swipeAction:(id)sender {
    [self hideActiveSnackBarView];
}

#pragma mark - Timer

- (void)startTimer {
    if (!self.timer) {
        NSTimeInterval interval = [self.snacksDatasource snacksWindowSnackBarVisibilityDuration:self];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(timerAction:) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
}

- (void)stopTimer {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - Interface

- (void)showSnackBarWithContentView:(UIView *)contentView hideActive:(BOOL)hideActive hideBlock:(void(^)())hideBlock {
    contentView.snackBarContentViewDidHideBlock = hideBlock;
    if (![self.snackBarContentViews containsObject:contentView]) {
        [self.snackBarContentViews addObject:contentView];
    }
    if (hideActive) {
        [self hideActiveSnackBarView];
    }
    if (!self.activeSnackBarContentView) {
        UIView *backgroundView = [[UIView alloc] init];
        backgroundView.backgroundColor = contentView.backgroundColor;
        [self addSubview:backgroundView];
        [backgroundView addSubview:contentView];
        
        contentView.snackBarBackgroundView = backgroundView;
        
        self.activeSnackBarContentView = contentView;
        
        contentView.snackBarContentViewVisible = NO;
        [self layoutSnackBarContentView:contentView];
        
        contentView.snackBarContentViewVisible = YES;
        
        UIViewAnimationOptions options = UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction;
        [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:0.0 options:options animations:^{
            [self layoutSnackBarContentView:contentView];
        } completion:^(BOOL finished) {
            [self startTimer];
        }];
    }
}

- (void)hideActiveSnackBarView {
    [self stopTimer];
    
    UIView *contentView = self.activeSnackBarContentView;
    contentView.snackBarContentViewVisible = NO;
    [self.snackBarContentViews removeObject:contentView];
    self.activeSnackBarContentView = nil;
    
    if (self.snackBarContentViews.count > 0) {
        UIView *lastContentView = self.snackBarContentViews.lastObject;
        [self showSnackBarWithContentView:lastContentView hideActive:NO hideBlock:lastContentView.snackBarContentViewDidHideBlock];
    }
    
    [UIView animateWithDuration:0.35 delay:0.0 usingSpringWithDamping:0.95 initialSpringVelocity:0.5 options:0 animations:^{
        [self layoutSnackBarContentView:contentView];
    } completion:^(BOOL finished) {
        [contentView.snackBarBackgroundView removeFromSuperview];
        [contentView removeFromSuperview];
        if (contentView.snackBarContentViewDidHideBlock) {
            contentView.snackBarContentViewDidHideBlock();
        }
        if (self.snackBarContentViews.count == 0) {
            if ([self.snacksDelegate respondsToSelector:@selector(snacksWindowDidHideLastSnackBar:)]) {
                [self.snacksDelegate snacksWindowDidHideLastSnackBar:self];
            }
        }
    }];
}

#pragma mark - HitTest

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGFloat offsetForSwipe = 20.0;
    CGRect rect = self.activeSnackBarContentView.snackBarBackgroundView.frame;
    rect.size.height = rect.size.height + offsetForSwipe;
    return CGRectContainsPoint(rect, point);
}

@end
