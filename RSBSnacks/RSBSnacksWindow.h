//
//  RSBSnacksWindow.h
//  Pods
//
//  Created by Evgeny Mikhaylov on 10/11/2016.
//
//

#import <UIKit/UIKit.h>

@class RSBSnacksWindow;

@protocol RSBSnacksWindowDatasource <NSObject>

- (NSTimeInterval)snacksWindowSnackBarVisibilityDuration:(RSBSnacksWindow *)window;

@end

@protocol RSBSnacksWindowDelegate <NSObject>

@optional
- (void)snacksWindowDidHideLastSnackBar:(RSBSnacksWindow *)window;

@end

@interface RSBSnacksWindow : UIWindow

@property (nonatomic, weak) id<RSBSnacksWindowDelegate> snacksDelegate;
@property (nonatomic, weak) id<RSBSnacksWindowDatasource> snacksDatasource;
@property (nonatomic, readonly) BOOL snackBarIsVisible;

- (void)showSnackBarWithContentView:(UIView *)contentView hideActive:(BOOL)hideActive hideBlock:(void(^)())hideBlock;
- (void)hideActiveSnackBarView;

@end
