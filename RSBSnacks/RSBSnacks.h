//
//  RSBSnackBar.h
//  BannersSample
//
//  Created by Evgeny Mikhaylov on 12/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBSnacks : NSObject

@property (nonatomic, class) BOOL snackBarsQueueEnabled;
@property (nonatomic, class) NSTimeInterval snackBarVisibilityDuration;
@property (nonatomic, class) UIWindowLevel maxSupportedWindowLevel;

+ (void)showSnackBarWithContentView:(UIView *)contentView;
+ (void)showSnackBarWithContentView:(UIView *)contentView hideBlock:(void(^)())hideBlock;
+ (void)hideActiveSnackBar;

@end
