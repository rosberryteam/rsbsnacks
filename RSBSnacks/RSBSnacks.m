//
//  RSBSnackBar.m
//  BannersSample
//
//  Created by Evgeny Mikhaylov on 12/10/2016.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "RSBSnacks.h"
#import "RSBSnacksWindow.h"
#import "RSBSnacksViewController.h"

@interface RSBSnacks () <RSBSnacksWindowDelegate, RSBSnacksWindowDatasource>

@property (nonatomic) RSBSnacksWindow *snacksWindow;

@property (nonatomic) BOOL snackBarsQueueEnabled;
@property (nonatomic) NSTimeInterval snackBarVisibilityDuration;
@property (nonatomic) UIWindowLevel maxSupportedWindowLevel;

@end

@implementation RSBSnacks

+ (RSBSnacks *)sharedInstance {
    static RSBSnacks *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.snackBarsQueueEnabled = NO;
        self.snackBarVisibilityDuration = 2.5;
        self.maxSupportedWindowLevel = UIWindowLevelNormal;
    }
    return self;
}

#pragma mark - Show/Hide

- (void)showSnackBarWithContentView:(UIView *)contentView hideBlock:(void(^)())hideBlock {
    if (!self.snacksWindow) {
        self.snacksWindow = [[RSBSnacksWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.snacksWindow.rootViewController = [[RSBSnacksViewController alloc] init];
        self.snacksWindow.snacksDelegate = self;
        self.snacksWindow.snacksDatasource = self;
        [self.snacksWindow makeKeyAndVisible];
    }
    [self.snacksWindow showSnackBarWithContentView:contentView hideActive:!self.snackBarsQueueEnabled hideBlock:hideBlock];
}

- (void)hideActiveSnackBar {
    [self.snacksWindow hideActiveSnackBarView];
}

#pragma mark - RSBSnackBarsContainerViewDatasource

- (NSTimeInterval)snacksWindowSnackBarVisibilityDuration:(RSBSnacksWindow *)window {
    return self.snackBarVisibilityDuration;
}

#pragma mark - RSBSnackBarContainerViewDelegate

- (void)snacksWindowDidHideLastSnackBar:(RSBSnacksWindow *)window {
    self.snacksWindow.hidden = YES;
    self.snacksWindow = nil;
}

#pragma mark - Interface

+ (void)setSnackBarsQueueEnabled:(BOOL)snackBarsQueueEnabled {
    [self sharedInstance].snackBarsQueueEnabled = snackBarsQueueEnabled;
}

+ (BOOL)snackBarsQueueEnabled {
    return [self sharedInstance].snackBarsQueueEnabled;
}

+ (void)setSnackBarVisibilityDuration:(NSTimeInterval)snackBarVisibilityDuration {
    [self sharedInstance].snackBarVisibilityDuration = snackBarVisibilityDuration;
}

+ (NSTimeInterval)snackBarVisibilityDuration {
    return [self sharedInstance].snackBarVisibilityDuration;
}

+ (void)setMaxSupportedWindowLevel:(UIWindowLevel)maxSupportedWindowLevel {
    [self sharedInstance].maxSupportedWindowLevel = maxSupportedWindowLevel;
}

+ (UIWindowLevel)maxSupportedWindowLevel {
    return [self sharedInstance].maxSupportedWindowLevel;
}

+ (void)showSnackBarWithContentView:(UIView *)contentView {
    [self showSnackBarWithContentView:contentView hideBlock:nil];
}

+ (void)showSnackBarWithContentView:(UIView *)contentView hideBlock:(void(^)())hideBlock {
    [[self sharedInstance] showSnackBarWithContentView:contentView hideBlock:hideBlock];
}

+ (void)hideActiveSnackBar {
    [[self sharedInstance] hideActiveSnackBar];
}

@end
