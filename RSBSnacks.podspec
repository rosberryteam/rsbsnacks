Pod::Spec.new do |s|

s.name                = "RSBSnacks"
s.version             = "0.0.3"
s.summary             = "iOS library for snack bar managing"
s.homepage            = "https://github.com/rosberry/RSBSnacks"
s.license             = 'MIT'
s.author              = { "Rosberry" => "info@rosberry.com" }
s.source              = { :git => "https://github.com/rosberry/RSBSnacks.git", :tag => "0.0.3" }
s.platform            = :ios, '8.0'
s.requires_arc        = true
s.source_files        = 'RSBSnacks/*.{h,m}'

end
